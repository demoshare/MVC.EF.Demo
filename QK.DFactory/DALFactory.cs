﻿using QK.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QK.DFactory
{
    public class DALFactory
    {
        static string dalLayer = WebSite.DALAssembly;
        public IDAL.IProduct CreateProduct()
        {
            return (IDAL.IProduct)Assembly.Load(dalLayer).CreateInstance(dalLayer + ".Product");
        }
    }
}
