﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Reflection;

namespace QK.Common
{
    /// <summary>
    /// Linq扩展类
    /// </summary>
    public static class LinqExpressions
    {
        public static Expression<Func<T, bool>> True<T>() { return f => true; }
        public static Expression<Func<T, bool>> False<T>() { return f => false; }
        
        /// <summary>
        /// List转DataTable
        /// </summary>
        /// <typeparam name="TResult">泛型</typeparam>
        /// <param name="ListValue">泛型IEnumerable对象</param>
        /// <returns></returns>
        public static DataTable ListToDataTable<TResult>(this IEnumerable<TResult> _ListValue)
        //where TResult : class, new()
        {
            IEnumerable<TResult> ListValue = _ListValue;

            //建立一個回傳用的 DataTable
            DataTable dt = new DataTable();
            //取得映射型別
            Type type = typeof(TResult);
            //宣告一個 PropertyInfo 陣列，來接取 Type 所有的共用屬性
            PropertyInfo[] PI_List = null;
            foreach (var item in ListValue)
            {
                //判斷 DataTable 是否已經定義欄位名稱與型態
                if (dt.Columns.Count == 0)
                {
                    //取得 Type 所有的共用屬性
                    PI_List = item.GetType().GetProperties();
                    //將 List 中的 名稱 與 型別，定義 DataTable 中的欄位 名稱 與 型別
                    foreach (var item1 in PI_List)
                    {
                        Type colType = item1.PropertyType;
                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }
                        dt.Columns.Add(item1.Name, colType);
                    }
                }

                //在 DataTable 中建立一個新的列
                DataRow dr = dt.NewRow();
                //將資料足筆新增到 DataTable 中
                foreach (var item2 in PI_List)
                {
                    dr[item2.Name] = item2.GetValue(item, null) == null ? DBNull.Value : item2.GetValue(item, null);
                }
                dt.Rows.Add(dr);
            }
            dt.AcceptChanges();
            return dt;
        }

        /// <summary>
        /// DataTable转List
        /// </summary>
        /// <typeparam name="TResult">泛型</typeparam>
        /// <param name="DataTableValue">数据表</param>
        /// <returns></returns>
        public static List<TResult> DataTableToList<TResult>(this DataTable DataTableValue) where TResult : class, new()
        {
            //建立一個回傳用的 List<TResult>
            List<TResult> Result_List = new List<TResult>();
            //取得映射型別
            Type type = typeof(TResult);
            //儲存 DataTable 的欄位名稱
            List<PropertyInfo> pr_List = new List<PropertyInfo>();
            foreach (PropertyInfo item in type.GetProperties())
            {
                if (DataTableValue.Columns.IndexOf(item.Name) != -1)
                    pr_List.Add(item);
            }
            //足筆將 DataTable 的值新增到 List<TResult> 中
            foreach (DataRow item in DataTableValue.Rows)
            {
                TResult tr = new TResult();
                foreach (PropertyInfo item1 in pr_List)
                {
                    if (item[item1.Name] != DBNull.Value)
                        item1.SetValue(tr, item[item1.Name], null);
                }
                Result_List.Add(tr);
            }
            return Result_List;
        }


        /// <summary>
        /// 条件表达式OR
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="expr1">条件表达式1</param>
        /// <param name="expr2">条件表达式2</param>
        /// <returns>新的条件表达式</returns>
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
        Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters);
            return Expression.Lambda<Func<T, bool>>
            (Expression.Or(expr1.Body, invokedExpr), expr1.Parameters);
        }

        /// <summary>
        /// 条件表达式AND
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="expr1">条件表达式1</param>
        /// <param name="expr2">条件表达式2</param>
        /// <returns>新的条件表达式</returns>
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
        Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters);
            return Expression.Lambda<Func<T, bool>>
            (Expression.And(expr1.Body, invokedExpr), expr1.Parameters);
        }

        /// <summary>
        /// IQueryable 分页扩展
        /// </summary>
        /// <typeparam name="TSource">数据源类型</typeparam>
        /// <param name="source">数据源</param>
        /// <param name="pageIndex">第几页</param>
        /// <param name="pageSize">一页多少数量</param>
        /// <returns>分页后数据源</returns>
        public static IQueryable<TSource> ListPage<TSource>(this IQueryable<TSource> source, int pageIndex, int pageSize)
        {
            return source.Skip(pageSize * (pageIndex - 1)).Take(pageSize);
        }

        /// <summary>
        /// IQueryable 分页扩展
        /// </summary>
        /// <typeparam name="TSource">数据源类型</typeparam>
        /// <param name="source">数据源</param>
        /// <param name="pageIndex">第几页</param>
        /// <param name="pageSize">一页多少数量</param>
        /// <param name="allCount">总数量</param>
        /// <returns>分页后数据源</returns>
        public static IQueryable<TSource> ListPage<TSource>(this IQueryable<TSource> source, int pageIndex, int pageSize, ref int allCount)
        {
            allCount = source.Count();
            return source.Skip(pageSize * (pageIndex - 1)).Take(pageSize);
        }

        public static PagedList<IEnumerable<TSource>> PagedList<TSource>(this IQueryable<TSource> source, int pageIndex, int pageSize)
        {
            int allCount = source.Count();
            var list = source.Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
            return new PagedList<IEnumerable<TSource>>
            {
                Total = allCount,
                Rows = list
            };
        }
        /// <summary>
        /// IEnumerable分页扩展
        /// </summary>
        /// <typeparam name="TSource">数据源类型</typeparam>
        /// <param name="source">数据源</param>
        /// <param name="pageIndex">第几页</param>
        /// <param name="pageSize">一页多少数量</param>
        /// <returns>分页后数据源</returns>
        public static IEnumerable<TSource> ListPage<TSource>(this IEnumerable<TSource> source, int pageIndex, int pageSize)
        {
            return source.Skip(pageSize * (pageIndex - 1)).Take(pageSize);
        }

        /// <summary>
        /// IEnumerable分页扩展
        /// </summary>
        /// <typeparam name="TSource">数据源类型</typeparam>
        /// <param name="source">数据源</param>
        /// <param name="pageIndex">第几页</param>
        /// <param name="pageSize">一页多少数量</param>
        /// <param name="allCount">总数量</param>
        /// <returns>分页后数据源</returns>
        public static IEnumerable<TSource> ListPage<TSource>(this IEnumerable<TSource> source, int pageIndex, int pageSize, ref int allCount)
        {
            allCount = source.Count();
            return source.Skip(pageSize * (pageIndex - 1)).Take(pageSize);
        }


        public static PagedList<IEnumerable<TSource>> PagedList<TSource>(this IEnumerable<TSource> source, int pageIndex, int pageSize)
        {
            int allCount = source.Count();
            var list =source.Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
            return new PagedList<IEnumerable<TSource>> {
                Total = allCount,
                Rows = list
            };
        }

        //public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
        //    this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        //{
        //    HashSet<TKey> seenKeys = new HashSet<TKey>();
        //    foreach (TSource element in source)
        //    {
        //        if (seenKeys.Add(keySelector(element))) { yield return element; }
        //    }
        //}

    }
}
