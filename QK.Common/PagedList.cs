﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.Common
{
    public class PagedList<T>
    {
        public int Total { get; set; }

        public T Rows { get; set; }
    }
}
