﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace QK.Framework
{
    public class BaseController : Controller
    {
        #region 提示信息

        public JsonResult JsonMsg(object obj)
        {
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult JsonMsg(int total, object obj)
        {
            if (total > 0)
                return Json(new { Rows = obj, Total = total }, JsonRequestBehavior.AllowGet);
            else
                return Json(@"{""Rows"":[],""Total"":""0""}", JsonRequestBehavior.AllowGet);

        }

        public JsonResult JsonMsg(BusinessCode code, object data = null)
        {
            return Json(new JsonMvcResult(code, "", data), JsonRequestBehavior.AllowGet);
        }
        public JsonResult JsonMsg(BusinessCode code, string msg = "", object data = null)
        {
            return Json(new JsonMvcResult(code, msg, data), JsonRequestBehavior.AllowGet);
        }

        #endregion


    }


}