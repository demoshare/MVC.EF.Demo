﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.Framework
{
    /// <summary>
    /// 统一返回格式
    /// </summary>
    public class JsonMvcResult
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        /// <param name="data"></param>
        public JsonMvcResult(BusinessCode code, string msg, object data)
        {
            this.Code = (int)code;
            this.Message = msg;
            this.Data = data;
        }

        public int Code { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
