﻿using QK.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.IBLL
{
    /// <summary>
    /// 业务逻辑层：产品约束接口
    /// </summary>
    public interface IProduct
    {
        /// <summary>
        /// 新增一条记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Add(Model.Product model);
        /// <summary>
        /// 更新一条记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Update(Model.Product model);
        /// <summary>
        /// 根据编号获取一条记录
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Model.Product GetModel(int ID);
        /// <summary>
        /// 根据编号删除一条记录
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool Delete(int ID);
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        bool Delete(int[] ids);
        /// <summary>
        /// 获取所有记录
        /// </summary>
        /// <returns></returns>
        IEnumerable<Model.Product> List();

        /// <summary>
        /// 获取分页记录记录
        /// </summary>
        PagedList<IEnumerable<Model.Product>> List(int pageIndex, int pageSize, string name);
    }
}
