﻿using QK.BFactory;
using QK.Common;
using QK.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QK.Web.Controllers
{
    public class ProductController : BaseController
    {
        private IBLL.IProduct bll = BLLFactory.CreateProduct();

        public ViewResult Index()
        {
            return View();
        }

        // **************************************
        // URL: /Product/AddProduct
        //添加
        // **************************************
        public JsonResult AddProduct(Model.Product model)
        {
            bool result = bll.Add(model);
            if (result)
                return JsonMsg(BusinessCode.Normal, "添加成功");
            else
                return JsonMsg(BusinessCode.ParameterError, "添加失败");
        }

        // **************************************
        // URL: /Product/GetModel
        //根据ID得到一条记录
        // **************************************
        public JsonResult GetModel(int ID)
        {
            var model = bll.GetModel(ID);
            return JsonMsg(BusinessCode.Normal, model);

        }

        // **************************************
        // URL: /Product/GetModel
        //修改
        // **************************************
        public JsonResult ModifyProduct(Model.Product model)
        {
            bool result = bll.Update(model);
            if (result)
                return JsonMsg(BusinessCode.Normal, "修改成功");
            else
                return JsonMsg(BusinessCode.ParameterError, "修改失败");
        }

        // **************************************
        // URL: /Product/GetModel
        //删除
        // **************************************
        public JsonResult DelProduct(string idStr)
        {
            bool result = false;
            if (idStr.Length == 1)
            {
                result = bll.Delete(int.Parse(idStr));
            }
            else
            {
                int[] idList = idStr.Split(',').Select(p=>p.ToInt()).ToArray();
                result = bll.Delete(idList);
            }
            if (result)
                return JsonMsg(BusinessCode.Normal, "删除成功");
            else
                return JsonMsg(BusinessCode.ParameterError, "删除失败");

        }
    }
}