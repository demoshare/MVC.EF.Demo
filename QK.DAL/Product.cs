﻿using EntityFramework.Extensions;
using QK.Common;
using QK.EFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.DAL
{
    /// <summary>
    /// 数据访问层：产品
    /// </summary>
    public class Product : IDAL.IProduct
    {

        private readonly AppedDbContext dbRead = DouDbContext.DBRead;
        private readonly AppedDbContext dbWrite = DouDbContext.DBWrite;

        /// <summary>
        /// 新增一条记录
        /// </summary>
        public bool Add(Model.Product model)
        {
            dbWrite.Products.Add(model);
            return dbWrite.SaveChanges() > 0;
        }
        /// <summary>
        /// 更新一条记录
        /// </summary>
        public bool Update(Model.Product model)
        {
            dbWrite.Entry(model).State = EntityState.Modified;
            return dbWrite.SaveChanges() > 0;
        }

        /// <summary>
        /// 根据编号获取一条记录
        /// </summary>
        public Model.Product GetModel(int ID)
        {
            return dbRead.Products.FirstOrDefault(p => p.ID == ID);
        }
        /// <summary>
        /// 根据编号删除一条记录
        /// </summary>
        public bool Delete(int ID)
        {
            dbRead.Products.Where(p => p.ID == ID).Delete();
            return dbRead.SaveChanges() > 0;
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        public bool Delete(int[] ids)
        {
            dbRead.Products.Where(p => ids.Contains(p.ID)).Delete();
            return dbRead.SaveChanges() > 0;
        }

        /// <summary>
        /// 获取所有记录
        /// </summary>
        public IEnumerable<Model.Product> List()
        {
            return dbRead.Products.ToList();
        }

        /// <summary>
        /// 获取分页记录记录
        /// </summary>
        public PagedList<IEnumerable<Model.Product>> List(int pageIndex, int pageSize, string name)
        {
            var list = from a in dbRead.Products select a;
            if (string.IsNullOrWhiteSpace(name))
                list = list.Where(p => p.Name.Contains(name));

            return list.OrderByDescending(p => p.ID).PagedList<Model.Product>(pageIndex, pageSize);

        }

    }

}
