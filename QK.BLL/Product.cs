﻿using QK.Common;
using QK.DFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.BLL
{
    public class Product : IBLL.IProduct
    {
        private readonly IDAL.IProduct dal = new DALFactory().CreateProduct();

        public Product() { }

        /// <summary>
        /// 新增一条记录
        /// </summary>
        public bool Add(Model.Product model)
        {
            return dal.Add(model);
        }
        /// <summary>
        /// 更新一条记录
        /// </summary>
        public bool Update(Model.Product model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 根据编号获取一条记录
        /// </summary>
        public Model.Product GetModel(int ID)
        {
            return dal.GetModel(ID);
        }
        /// <summary>
        /// 根据编号删除一条记录
        /// </summary>
        public bool Delete(int ID)
        {
            return dal.Delete(ID);

        }
        /// <summary>
        /// 批量删除
        /// </summary>
        public bool Delete(int[] ids)
        {
            return dal.Delete(ids);
        }

        /// <summary>
        /// 获取所有记录
        /// </summary>
        public IEnumerable<Model.Product> List()
        {
            return dal.List();
        }

        /// <summary>
        /// 获取分页记录记录
        /// </summary>
        public PagedList<IEnumerable<Model.Product>> List(int pageIndex, int pageSize, string name)
        {
            return dal.List(pageIndex, pageSize, name);
        }
    }

}
