﻿using QK.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QK.BFactory
{
    public sealed class BLLFactory
    {
        private readonly static string bllLayer = WebSite.BLLAssembly;

        public static IBLL.IProduct CreateProduct()
        {
            return (IBLL.IProduct)Assembly.Load(bllLayer).CreateInstance(bllLayer + ".Product");
        }
    }
}
