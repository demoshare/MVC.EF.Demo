﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.Model
{
    /// <summary>
    /// 产品实体
    /// </summary>
    [Serializable]
    public class Product
    {
        public Product() { }

        private DateTime _CreateDate = DateTime.Now;

        public int ID { set; get; }
        public string Name { set; get; }
        public decimal Price { set; get; }
        public DateTime CreateDate { set { value = _CreateDate; } get { return _CreateDate; } }
    }
}
