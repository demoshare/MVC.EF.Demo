﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.Config
{
    public class WebSite
    {
        /// <summary>
        /// 读数据库链接
        /// </summary>
        public static string SEFConnection
        {
            get { return ConfigurationManager.AppSettings["SEFConnection"].ToString().Trim(); }
        }
        /// <summary>
        /// CUD数据库连接
        /// </summary>
        public static string CUDEFConnection
        {
            get { return ConfigurationManager.AppSettings["CUDEFConnection"].ToString().Trim(); }
        }

        public static string BLLAssembly
        {
            get { return ConfigurationManager.AppSettings["BLLLayer"].ToString().Trim(); }
        }

        public static string DALAssembly
        {
            get { return ConfigurationManager.AppSettings["DALLayer"].ToString().Trim(); }
        }
    }
}
