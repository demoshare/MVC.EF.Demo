﻿using QK.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.EFramework
{
    /// <summary>
    /// 管理数据库链接字符串
    /// </summary>
    public class DouDbContext
    {
        public static AppedDbContext DBRead
        {
            get
            {
                return new AppedDbContext(WebSite.SEFConnection);
            }
        }

        public static AppedDbContext DBWrite
        {
            get
            {
                return new AppedDbContext(WebSite.CUDEFConnection);
            }
        }
    }

}
