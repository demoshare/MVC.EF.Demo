﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using QK.Model.Mapping;

namespace QK.EFramework
{
    public class AppedDbContext : DbContext
    {

        public AppedDbContext(string connection) : base(connection) { }

        public DbSet<Model.Product> Products { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add(new ProductMap());


            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名的契约

            modelBuilder.Conventions.Remove<IncludeMetadataConvention>();//防止黑幕交易 要不然每次都要访问 EdmMetadata这个表
        }
    }

}
